import java.util.ArrayList;
import java.util.List;

public class TestDemo {
    public Boolean checkEmail(String userEmail) throws IncorrectEmailException {
        if (userEmail.contains("@") && (userEmail.contains("."))) {
            System.out.println("Проверено!");
            return true;
        } else {
            throw new IncorrectEmailException();
        }
    }

    public Boolean checkNumber(Integer number){
        if (number %2 ==0) {
            return true;
        }
        else {
            return false;
        }
    }

    public List<String> checkStrings(List<String> strings) {
        List<String> result = new ArrayList<>();
        for (String string: strings) {
            if (string.length() <=6 || string.startsWith("a")) {
                result.add(string);
            }
        }
        return result;
    }
}
