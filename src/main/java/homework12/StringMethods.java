package homework12;

import static org.apache.commons.lang3.StringUtils.reverse;
public class StringMethods {
    public String addBracketsForBeauty(String input) throws IncorrectInputException {
        if (input.length() == 0) {
            throw new IncorrectInputException();
        } else {
            StringBuilder beautyStrings = new StringBuilder();
            for (String s : input.split(" ")) {
                beautyStrings.append("[").append(s).append("] ");
            }
            return beautyStrings.toString().trim();
        }
    }

    public String  reverseString(String stringToReverse) {
        if (stringToReverse.length() == 0) {
            return null;
        } else {
            return reverse(stringToReverse);
        }
    }
}
