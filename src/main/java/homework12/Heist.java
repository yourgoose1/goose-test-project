package homework12;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class Heist extends Vault {
    public static void main(String[] args) {

        Vault federalVault = new Vault();
        System.out.println("Состояние хранилища: " + federalVault);

        Vault myVault = new Vault();
        int stolenDollars;
        int stolenEuros;
        double stolenTonsOfGold;
        String stolenPentagonNukesCodes;

        try {
            Field dollars = federalVault.getClass().getDeclaredField("dollars");
            dollars.setAccessible(true);
            stolenDollars = (int) dollars.get(federalVault);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        try {
            Field euros = federalVault.getClass().getDeclaredField("euros");
            euros.setAccessible(true);
            stolenEuros = (int) euros.get(federalVault);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        try {
            Field tonsOfGold = federalVault.getClass().getDeclaredField("tonsOfGold");
            tonsOfGold.setAccessible(true);
            stolenTonsOfGold = (double) tonsOfGold.get(federalVault);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        try {
            Field pentagonNukesCodes = federalVault.getClass().getDeclaredField("pentagonNukesCodes");
            pentagonNukesCodes.setAccessible(true);
            stolenPentagonNukesCodes = (String) pentagonNukesCodes.get(federalVault);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        Field[] declaredFields = federalVault.getClass().getDeclaredFields();
        Arrays.stream(declaredFields).forEach(field -> {
            try {
                field.setAccessible(true);
                field.set(federalVault, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Arrays.stream(declaredFields).forEach(field -> {
            try {
                field.setAccessible(true);
                field.set(federalVault, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        System.out.println("Упс, вас хакнули. Теперь у вас: " + federalVault);

        try {
            Constructor<?> mineVault = federalVault.getClass().getDeclaredConstructor(int.class, int.class, double.class, String.class);
            mineVault.setAccessible(true);
            myVault = (Vault) mineVault.newInstance(stolenDollars, stolenEuros, stolenTonsOfGold, stolenPentagonNukesCodes);
            System.out.println("Уезжаем в Майми имея при себе: " + myVault.toString());
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                 InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
