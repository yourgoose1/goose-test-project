
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class Test11 {

    TestDemo testDemo = new TestDemo();

    @Test
    public void emailCheckTest1() throws IncorrectEmailException {
        Assert.assertTrue(testDemo.checkEmail("salampopolam@gmail.com"));
    }

    @Test
    public void emailCheckTest2(){
        Assert.assertFalse(false, "Invalid email address");
    }

    @Test
    public void checkNumberTest1(){
        Assert.assertFalse(testDemo.checkNumber(3));
    }

    @DataProvider(name = "evenNumbersProvider")
    public static Object[][] evenNumbersProvider() {
        return  new Object[][] {
                {44},
                {24},
                {20},
                {88},
                {100},
        };
    }

    @Test(dataProvider = "evenNumbersProvider")
    public void checkNumberTest2(Integer number){
        Assert.assertTrue(testDemo.checkNumber(number));
    }

    @Test
    public void checkList1(){
        List<String> usersString = Arrays.asList("Раппав", "абвгде", "тыктык");
        List<String> testResult = testDemo.checkStrings(usersString);
        Assert.assertEquals(testResult, Arrays.asList("Раппав", "абвгде", "тыктык"));
    }

    @Test
    public void checkList2(){
        List<String> usersString = Arrays.asList("аф", "1234");
        List<String> testResult = testDemo.checkStrings(usersString);
        Assert.assertEquals(testResult, Arrays.asList("аф", "1234"));
    }
}
