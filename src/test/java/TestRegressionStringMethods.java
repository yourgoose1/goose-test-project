import homework12.IncorrectInputException;
import homework12.StringMethods;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestRegressionStringMethods {

    StringMethods stringMethods = new StringMethods();
//    IncorrectInputException incorrectInputException = new IncorrectInputException();
    String input;
    String expectedResult;
    String stringWithBrackets;

    @Test
    public void bracketsPositiveTest() throws IncorrectInputException {
        input = "Давай сделай красиво";
        expectedResult = "[Давай] [сделай] [красиво]";
        stringWithBrackets = stringMethods.addBracketsForBeauty(input);
        Assert.assertEquals(expectedResult, stringWithBrackets);
    }

    @Test
    public void bracketsPositiveTestWithNumbers() throws IncorrectInputException {
        input = "1";
        expectedResult = "[1]";
        stringWithBrackets = stringMethods.addBracketsForBeauty(input);
        Assert.assertEquals(expectedResult, stringWithBrackets);
    }

    @Test
    public void bracketsNegativeTest() {
        input = "";
        boolean exceptionThrow = false;
        try {
            stringWithBrackets = stringMethods.addBracketsForBeauty(input);
        } catch (IncorrectInputException e) {
            exceptionThrow = true;
        }
        Assert.assertTrue(exceptionThrow);
    }

    @Test
    public void reverseStringPositiveTest(){
        input = "А лес у села";
        expectedResult = "алес у сел А";
        Assert.assertEquals(expectedResult, stringMethods.reverseString(input));
    }

    @Test
    public void reverseStringWithNumbersTest(){
        input = "88005553535";
        expectedResult = "53535550088";
        Assert.assertEquals(expectedResult, stringMethods.reverseString(input));
    }

    @Test
    public void reverseStringNegativeTest(){
        input = "";
        Assert.assertNull(stringMethods.reverseString(input));
    }



}
