import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

public class TestLitres {
    SelenideElement searchBar = Selenide.$x("//input[@data-test-id=\"header__search-input--desktop\"]");
    SelenideElement searchBtn = Selenide.$x("//button[@data-test-id=\"header__search-button--desktop\"]");
    SelenideElement firstElement = Selenide.$x("//img[@alt=\"Грокаем функциональное мышление\"]");
    SelenideElement price = Selenide.$x("//strong[contains(text(), '799')]");
    SelenideElement addToBuy = Selenide.$x("//div[contains(text(), 'Добавить в корзину')]");

    @Test
    public void openLitres(){
        Selenide.open("https://www.litres.ru/");
        searchBar.setValue("грокаем алгоритмы");
        searchBtn.click();
        firstElement.click();
        price.should(Condition.visible);
//        цены по абонементу на сайте нет(
        addToBuy.should((Condition.visible));
    }
}
