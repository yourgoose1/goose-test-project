import homework12.IncorrectInputException;
import homework12.StringMethods;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestSmokeStringMethods {
    StringMethods stringMethods = new StringMethods();
    String input;
    String expectedResult;
    String stringWithBrackets;

    @Test
    public void bracketsPositiveTest() throws IncorrectInputException {
        input = "Давай сделай красиво";
        expectedResult = "[Давай] [сделай] [красиво]";
        stringWithBrackets = stringMethods.addBracketsForBeauty(input);
        Assert.assertEquals(expectedResult, stringWithBrackets);
    }

    @Test
    public void reverseStringPositiveTest(){
        input = "А лес у села";
        expectedResult = "алес у сел А";
        Assert.assertEquals(expectedResult, stringMethods.reverseString(input));
    }
}
