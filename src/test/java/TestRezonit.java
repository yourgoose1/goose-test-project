import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

public class TestRezonit {
    SelenideElement printedCircuitBoards = Selenide.$x("//header/div[2]/div/div/ul/li[1]");
    SelenideElement inputLength = Selenide.$x("//input[@id='calculator-input-1']");
    SelenideElement inputWidth = Selenide.$x("//input[@id='calculator-input-2']");
    SelenideElement inputQuantity = Selenide.$x("//input[@id='calculator-input-3']");
    SelenideElement submitBtn = Selenide.$x("//button[@id='calculate']");
    SelenideElement totalPrice = Selenide.$x("//span[@id='total-price']");
    SelenideElement alert = Selenide.$x("//div[@class='alert alert-danger']");


    @Test
    public void testRezonitPositive(){
        Selenide.open("https://www.rezonit.ru/");
        printedCircuitBoards.click();
        inputLength.setValue("10");
        inputWidth.setValue("20");
        inputQuantity.setValue("20");
        submitBtn.click();;
        Selenide.sleep(5000);
        totalPrice.should(Condition.visible);
    }

    @Test
    public void testRezonitNegative(){
        Selenide.open("https://www.rezonit.ru/");
        printedCircuitBoards.click();
        inputLength.setValue("10");
        inputWidth.setValue("-999");
        alert.should(Condition.visible);
    }
}
